//
//  LoginPage.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

final class LoginPage: BaseView {
    
    //MARK: - Properties
    var didTappedSendButton: ((_ requestModel: LoginRequestModel)->())?
    
    //MARK: - UI Properties
    private let textFieldUsername: CustomTextField = {
        let textField = CustomTextField(frame: .zero)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Email  giriniz"
        return textField
    }()
    
    private let textFieldPassword: CustomTextField = {
        let textField = CustomTextField(frame: .zero)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Sifre giriniz"
        textField.isSecureTextEntry = true
        return textField
    }()
    
    private lazy var buttonLogin: UIButton = {
        let button = UIButton(type:.system)
        button.setTitle("Login", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .navyBlue
        button.addTarget(self, action: #selector(buttonSendTapped), for: .touchUpInside)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        button.layer.cornerRadius = 21
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    //MARK: - Override Functions
    override func configureUI() {
        super.configureUI()
        configureScrollView()
    }
    
    override func setupStackView() {
        super.setupStackView()
        stackView.addArrangedSubview(textFieldUsername)
        stackView.addArrangedSubview(textFieldPassword)
        stackView.addArrangedSubview(buttonLogin)
        textFieldUsername.heightAnchor.constraint(equalToConstant: 44).isActive = true
        textFieldPassword.heightAnchor.constraint(equalToConstant: 44).isActive = true
        buttonLogin.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
    
    //MARK: - Actions
    @objc private func buttonSendTapped() {
        guard let emailText = self.textFieldUsername.text , let passwordText = self.textFieldPassword.text else { return }
        guard emailText.count > 0 && passwordText.count > 0 else {
            return
        }
        let requestModel = LoginRequestModel(username: emailText.lowercased(), password: passwordText)
        self.didTappedSendButton?(requestModel)
    }
}
