//
//  LoginService.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

final class LoginService: BaseService<LoginRequestModel, LoginResponseModel> {
    
    override init() {
        super.init()
        path = "user-service/login"
        showActivityIndicator = true
        method = .post
    }
}
