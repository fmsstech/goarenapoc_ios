//
//  LoginVM.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

final class LoginVM: BaseVM {
    
    private var responseModel : LoginResponseModel? {
        didSet{
            guard let responseModel = self.responseModel else { return }
            UserDefaults.standard.setToken(value: responseModel.token ?? "")
            UserDefaults.standard.saveLoginResponseModel(loginResponseModel: responseModel)
        }
    }
    
    override var screenType: ScreenType {
        .login
    }
    
    override func configureInit() {
        super.configureInit()
        
    }
    
    func startLogin(requestModel : LoginRequestModel) {
        LoginService().setRequestModel(requestModel).response { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                self.responseModel = response
                self.state?(.success)
            case .failure:
                self.state?(.failure(nil))
            }
        }
    }
}
