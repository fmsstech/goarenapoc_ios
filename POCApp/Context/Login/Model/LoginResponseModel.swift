//
//  LoginResponseModel.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

struct LoginResponseModel: Codable {
    var token: String?
    var username: String?
    var id : Double?
}
