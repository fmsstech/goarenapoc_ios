//
//  LoginRequestModel.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

struct LoginRequestModel : BaseRequestProtocol {
    var username : String?
    var password : String?
}
