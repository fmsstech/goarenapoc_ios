//
//  DashboardVC.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

final class DashboardVC: BaseVC {
    
    //MARK: - Properties
    private var viewModel : DashboardVM = DashboardVM()
    
    //MARK: - UI Properties
    private let viewPage: DashboardPage = DashboardPage()
    
    override var navBarColor: UIColor {
        .midBlue
    }
    
    override var headerTitleColor: UIColor {
        .white
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        headerTitle = viewModel.screenType.screenName
        viewPage.viewModel = self.viewModel
    }
    
    //MARK: - Override Functions
    override func configureUI() {
        super.configureUI()
        self.view.backgroundColor = .whiteSmoke
        view.addSubview(viewPage)
        viewPage.anchor(top: headerBottomAnchor,
                        leading: self.view.leadingAnchor,
                        bottom: self.view.bottomAnchor,
                        trailing: self.view.trailingAnchor,
                        paddingTop: 0, paddingleft: 0,
                        paddingBottom: 0, paddingRight: 0,
                        width: 0, height: 0,
                        centerX: nil,
                        centerY: nil)
    }
}
