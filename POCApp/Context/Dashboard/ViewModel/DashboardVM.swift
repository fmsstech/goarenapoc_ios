//
//  DashboardVM.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

final class DashboardVM: BaseVM {
    
    private(set) var players = ["Onyekuru", "Muslera", "Falcao", "Belhanda", "Marcão", "Feghouli" ]
    private(set) var goals  = [7, 14, 7, 8, 12, 12]
    
    override var screenType: ScreenType {
        .dashboard
    }
}
