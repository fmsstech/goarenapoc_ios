//
//  DashboardPage.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit
import Charts

final class DashboardPage : BaseView {
    
    //MARK: - Properties
    var viewModel : DashboardVM? {
        didSet {
            guard let viewModel = self.viewModel else { return }
            self.customizeChart(dataPoints: viewModel.players, values: viewModel.goals.map{ Double($0)})
        }
    }
    
    //MARK: - UI Properties
    private let viewPieChart: PieChartView = PieChartView()
    private let viewBarChart: BarChartView = BarChartView()
    private let viewLineChart: LineChartView = LineChartView()
    
    //MARK: - Override Functions
    override func configureUI() {
        super.configureUI()
        configureScrollView()
    }
    
    override func setupStackView() {
        super.setupStackView()
        
        stackView.addArrangedSubview(viewPieChart)
        stackView.addArrangedSubview(viewBarChart)
        stackView.addArrangedSubview(viewLineChart)
        viewPieChart.heightAnchor.constraint(equalToConstant: 340).isActive = true
        viewBarChart.heightAnchor.constraint(equalToConstant: 340).isActive = true
        viewLineChart.heightAnchor.constraint(equalToConstant: 360).isActive = true
    }
    
    //MARK: - Private Functions
    private func customizeChart(dataPoints: [String], values: [Double]) {
        
        var chartDataEntry: [ChartDataEntry] = []
        for i in 0..<dataPoints.count {
            let dataEntry = PieChartDataEntry(value: values[i], label: dataPoints[i], data: dataPoints[i] as AnyObject)
            chartDataEntry.append(dataEntry)
        }
        let pieChartDataSet = PieChartDataSet(entries: chartDataEntry, label: "Pie Chart View")
        pieChartDataSet.colors = colorsOfCharts(numbersOfColor: dataPoints.count)
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        let format = NumberFormatter()
        format.numberStyle = .none
        let formatter = DefaultValueFormatter(formatter: format)
        pieChartData.setValueFormatter(formatter)
        viewPieChart.data = pieChartData
        
        
        var barChartDataEntry: [BarChartDataEntry] = []
        for i in 0..<dataPoints.count {
          let dataEntry = BarChartDataEntry(x: Double(i), y: Double(values[i]))
            barChartDataEntry.append(dataEntry)
        }
        let chartDataSet = BarChartDataSet(entries: barChartDataEntry, label: "Bar Chart View")
        let chartData = BarChartData(dataSet: chartDataSet)
        viewBarChart.data = chartData
        
        var lineChartDataEntry: [ChartDataEntry] = []
        for i in 0..<dataPoints.count {
          let dataEntry = ChartDataEntry(x: values[i], y: Double(i))
            lineChartDataEntry.append(dataEntry)
        }
        let lineChartDataSet = LineChartDataSet(entries: lineChartDataEntry, label: "Line Chart View")
        let lineChartData = LineChartData(dataSet: lineChartDataSet)
        viewLineChart.data = lineChartData
    }
    
    private func colorsOfCharts(numbersOfColor: Int) -> [UIColor] {
        var colors: [UIColor] = []
        for _ in 0..<numbersOfColor {
            colors.append(UIColor.random)
        }
        return colors
    }
}
