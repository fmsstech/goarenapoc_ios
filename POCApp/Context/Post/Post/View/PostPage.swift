//
//  CreatePostPage.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

protocol CreatePostPageDelegate : NSObjectProtocol {
    func didTappedSelectImageButton()
    func didTappedShareButton()
}

class PostPage: BaseView  , UITextViewDelegate , UITextFieldDelegate {
    
    //MARK: - Properties
    var viewModel : PostVM? {
        didSet {
            updateUI()
        }
    }
    
    weak var delegate : CreatePostPageDelegate?
    
    //MARK: - UI Properties
    private lazy var textFieldTitle: CustomTextField = {
        let textField = CustomTextField(frame: .zero)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Baslik giriniz"
        textField.delegate = self
        return textField
    }()
    
    private lazy var textViewDesc: UITextView = {
        let tv = UITextView(frame: .zero)
        tv.layer.borderColor = UIColor.lightGrey.cgColor
        tv.layer.borderWidth = 1
        tv.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        tv.textColor = .sapphire
        tv.font = UIFont.systemFont(ofSize: 13, weight: .medium)
        tv.backgroundColor = .whiteSmoke
        tv.layer.cornerRadius = 4
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.delegate = self
        return tv
    }()
    
    let imageViewSelect: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.backgroundColor = .red
        imageView.layer.cornerRadius = 21
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var buttonShare: UIButton = {
        let button = UIButton(type:.system)
        button.setTitle("Paylas", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .navyBlue
        button.addTarget(self, action: #selector(buttonShareTapped), for: .touchUpInside)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        button.layer.cornerRadius = 12
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var buttonSelect: UIButton = {
        let button = UIButton(type:.system)
        button.setTitle("Select Image", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .navyBlue
        button.addTarget(self, action: #selector(buttonSelectImageTapped), for: .touchUpInside)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.cornerRadius = 4
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private var viewContainerImage : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    //MARK: - Override Functions
    override func configureUI() {
        super.configureUI()
        configureScrollView()
    }
    
    //MARK: - Private Functions
    internal override func setupStackView() {
        super.setupStackView()
        stackView.arrangedSubviews.forEach({$0.removeFromSuperview()})
        stackView.addArrangedSubview(textFieldTitle)
        stackView.addArrangedSubview(textViewDesc)
        stackView.addArrangedSubview(viewContainerImage)
        stackView.addArrangedSubview(buttonShare)
        textFieldTitle.heightAnchor.constraint(equalToConstant: 44).isActive = true
        textViewDesc.heightAnchor.constraint(equalToConstant: 120).isActive = true
        viewContainerImage.heightAnchor.constraint(equalToConstant: 140).isActive = true
        buttonShare.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        viewContainerImage.addSubview(imageViewSelect)
        viewContainerImage.addSubview(buttonSelect)
        imageViewSelect.anchor(top: viewContainerImage.topAnchor,
                                 leading: viewContainerImage.leadingAnchor,
                                 bottom: viewContainerImage.bottomAnchor,
                                 trailing: nil,
                                 paddingTop: 0,
                                 paddingleft: 0,
                                 paddingBottom: 0,
                                 paddingRight: 0,
                                 width: 140, height: 0,
                                 centerX: nil, centerY: nil)
        buttonSelect.anchor(top: viewContainerImage.topAnchor,
                                 leading: imageViewSelect.trailingAnchor,
                                 bottom: nil,
                                 trailing: nil,
                                 paddingTop: 40,
                                 paddingleft: 20,
                                 paddingBottom: 0,
                                 paddingRight: 0,
                                 width: 120, height: 0,
                                 centerX: nil, centerY: nil)
    }
    
    //MARK: - Actions
    @objc func buttonShareTapped() {
        guard textFieldTitle.text?.count ?? 0 > 0 , textViewDesc.text.count > 0 else { return }
        delegate?.didTappedShareButton()
    }
    
    @objc func buttonSelectImageTapped() {
        delegate?.didTappedSelectImageButton()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }

    //MARK: - Private Functions
    private func updateUI() {
        self.textFieldTitle.text = viewModel?.activePost.title
        self.textViewDesc.text = viewModel?.activePost.description
        self.imageViewSelect.setImageWithBase64String(imageStr: viewModel?.activePost.imageStr)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        viewModel?.request.description = textView.text
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewModel?.request.title = textField.text
    }
}
