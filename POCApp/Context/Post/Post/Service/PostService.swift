//
//  PostService.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

class PostService: BaseService<PostRequestModel, Post> {
    
    override init() {
        super.init()
        path = "post-service/post"
        showActivityIndicator = true
        method = .post
    }
}
