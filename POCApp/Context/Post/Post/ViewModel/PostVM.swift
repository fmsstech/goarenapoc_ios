//
//  PostVM.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

class PostVM: BaseVM {
    
    var request: PostRequestModel = PostRequestModel()
    
    override var screenType: ScreenType {
        .createPost
    }
    
    var activePost : Post = Post() {
        didSet {
            request.id = activePost.id
            request.imageStr = activePost.imageStr
            request.description = activePost.description
            request.title = activePost.title
        }
    }
    
    
    func createPost() {
    
        PostService()
            .setRequestModel(request)
            .response { [weak self](result) in
                guard let self = self else { return }
                switch result {
                case .success(_):
                    self.state?(.success)
                case .failure:
                    self.state?(.failure(nil))
                }
            }
    }
}
