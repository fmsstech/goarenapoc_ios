//
//  PostRequestModel.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

struct PostRequestModel: BaseRequestProtocol {
    
    var id: Double?
    var title: String?
    var description: String?
    var imageStr: String?
}
