//
//  PostVC.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit
import CropViewController

class PostVC: BaseVC {
    
    //MARK: - Properties
    var viewModel : PostVM = PostVM()
    
    //MARK: - UI Properties
    private lazy var viewPage : PostPage = {
        let view = PostPage()
        view.delegate = self
        return view
    }()
    
    override var navBarColor: UIColor {
        .midBlue
    }
    
    override var headerTitleColor: UIColor {
        .white
    }
    
    override var closeButtonColor: UIColor {
        .white
    }
    
    override var isCloseButtonHidden: Bool {
        false
    }
    
    init(viewModel : PostVM) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        headerTitle = viewModel.screenType.screenName
        bindUI()
        viewPage.viewModel = self.viewModel
    }
    
    //MARK: - Override Functions
    override func configureUI() {
        super.configureUI()
        self.view.backgroundColor = .whiteSmoke
        view.addSubview(viewPage)
        viewPage.anchor(top: headerBottomAnchor,
                        leading: self.view.leadingAnchor,
                        bottom: self.view.bottomAnchor,
                        trailing: self.view.trailingAnchor,
                        paddingTop: 0, paddingleft: 0,
                        paddingBottom: 0, paddingRight: 0,
                        width: 0, height: 0,
                        centerX: nil,
                        centerY: nil)
    }
    
    //MARK: - Private Functions
    private func bindUI() {
        viewModel.state = { [weak self] (result) in
            guard let self = self else { return }
            if result == StateType.success {
                self.updateUI()
            }
            else {
                print("Show Alert")
            }
        }
    }
    
    private func updateUI() {
        AppNotifications.updateHomeVC.post()
        self.dismiss(animated: true, completion: nil)
    }
    
    private func startPickerVC() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = false
        imagePickerController.modalPresentationStyle = .fullScreen
        imagePickerController.delegate = self
        
        AppHUD.shared.show()
        present(imagePickerController, animated: true) {
            AppHUD.shared.hide()
        }
    }
    
    private func presentCropViewController(image : UIImage) {
        let cropViewController = CropViewController(croppingStyle: .circular, image: image)
        cropViewController.modalPresentationStyle = .fullScreen
        cropViewController.delegate = self
        AppHUD.shared.show()
        present(cropViewController, animated: true) {
            AppHUD.shared.hide()
        }
    }
}

extension PostVC: CreatePostPageDelegate ,  UIImagePickerControllerDelegate & UINavigationControllerDelegate , CropViewControllerDelegate {
    
    func didTappedSelectImageButton() {
        startPickerVC()
    }
    
    func didTappedShareButton() {
        viewModel.createPost()
    }
    
    //MARK:- UIImageViewControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editingImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            print(editingImage)
        }
        else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            dismiss(animated: true, completion: nil)
            presentCropViewController(image: originalImage)
        }
    }
    
    //MARK:- CropViewControllerDelegate
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        guard let imageData: Data = image.jpegData(compressionQuality: 0.4) else {
            dismiss(animated: true, completion: nil)
            return }
        self.viewPage.imageViewSelect.image = image
        let imageStr : String = imageData.base64EncodedString(options: .endLineWithLineFeed)
        self.viewModel.request.imageStr = imageStr
        self.dismiss(animated: true,completion: nil)
    }
}
