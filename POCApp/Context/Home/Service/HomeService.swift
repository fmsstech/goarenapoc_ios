//
//  HomeService.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

final class HomeService: BaseService<HomeRequestModel, HomeResponseModel > {
    
    override init() {
        super.init()
        path = "post-service/post/find-all-confirmed"
        showActivityIndicator = true
        method = .post
    }
}
