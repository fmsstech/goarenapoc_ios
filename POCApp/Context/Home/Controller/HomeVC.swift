//
//  HomeVC.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

final class HomeVC: BaseVC {
    
    //MARK: - Properties
    private var viewModel: HomeVM = HomeVM()
    private var viewPage: HomePage = HomePage()
    
    override var navBarColor: UIColor {
        .midBlue
    }
    
    override var headerTitleColor: UIColor {
        .white
    }
    
    override var backButtonColor: UIColor {
        .white
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        headerTitle = viewModel.screenType.screenName
        bindUI()
        viewModel.fetchPostList()
        viewPage.viewModel = self.viewModel
    }
  
    //MARK: - Override Functions
    override func configureUI() {
        super.configureUI()
        self.view.backgroundColor = .whiteSmoke
        view.addSubview(viewPage)
        viewPage.anchor(top: headerBottomAnchor,
                        leading: self.view.leadingAnchor,
                        bottom: self.view.bottomAnchor,
                        trailing: self.view.trailingAnchor,
                        paddingTop: 0, paddingleft: 0,
                        paddingBottom: 0, paddingRight: 0,
                        width: 0, height: 0,
                        centerX: nil,
                        centerY: nil)
        AppNotifications.updateHomeVC.addObserver(observer: self, selector: #selector(updateUIWithObserver))
    }
    
    //MARK: - Private Functions
    private func bindUI() {
        viewModel.state = { [weak self] (result) in
            guard let self = self else { return }
            if result == StateType.success {
                self.updateUI()
            }
            else {
                print("Show Alert")
            }
        }
    }

    private func updateUI() {
        viewPage.updateUI()
    }
    
    @objc private func updateUIWithObserver() {
        viewModel.fetchPostList()
    }
}
