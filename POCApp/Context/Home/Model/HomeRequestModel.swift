//
//  HomeRequestModel.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

struct HomeRequestModel: BaseRequestProtocol  {
    var page : Double = 0
    var state : String = "WAITING_CONFIRM"
}
