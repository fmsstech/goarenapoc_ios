//
//  HomeResponseModel.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

struct HomeResponseModel : Decodable {
    let groupList: [GroupList]?
}

struct GroupList : Decodable {
    let post: Post?
    let user: User?
}

struct User: Decodable {
    let id : Double?
    let username: String?
    let createdBy: String?
    let lastModifiedDate: Double?
    let createdDate: Double?
    let deviceId: String?
    let lastLogin : Double?
}

struct Post: Decodable {
    var createdBy: String?
    var createdDate: Double?
    var description: String?
    var id: Double?
    var imageStr: String?
    var lastModifiedDate: Double?
    var state: String?
    var title: String?
    var userId: Double?
}
