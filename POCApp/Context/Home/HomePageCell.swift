//
//  HomePageCell.swift
//  POCApp
//
//  Created by Yusuf Cinar on 2.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

final class HomePageCell: UICollectionViewCell {
    
    //MARK: - Properties
    var viewModel: HomeItemVM? {
        didSet {
            self.updateUI()
        }
    }
    
    //MARK: - UI Properties
    private let viewContainer : UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = .white
        return view
    }()
    
    private let labelName : UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .left
        label.textColor = UIColor.baseText
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    private let labelTitle : UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.baseText
        return label
    }()
    
    private let labelDesc : UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.baseText
        return label
    }()
    
    private let imageViewPost : UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.backgroundColor = .red
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var buttonEdit: UIButton = {
        let button = UIButton(type:.system)
        button.setTitle("Edit", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(buttonEditTapped), for: .touchUpInside)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Private Functions
    private func configureUI() {
        addSubview(labelName)
        addSubview(labelTitle)
        addSubview(labelDesc)
        addSubview(imageViewPost)
        addSubview(labelTitle)
        addSubview(buttonEdit)
        
        labelName.anchor(top: self.topAnchor,
                         leading: self.leadingAnchor,
                         bottom: nil, trailing: nil,
                         paddingTop: 4, paddingleft: 12,
                         paddingBottom: 0, paddingRight: 0,
                         width: 0, height: 0,
                         centerX: nil, centerY: nil)
        
        labelTitle.anchor(top: labelName.bottomAnchor,
                          leading: self.leadingAnchor,
                          bottom: nil, trailing: nil,
                          paddingTop: 10, paddingleft: 12,
                          paddingBottom: 0, paddingRight: 0,
                          width: 0, height: 0,
                          centerX: nil, centerY: nil)
        
        labelDesc.anchor(top: labelTitle.bottomAnchor,
                         leading: self.leadingAnchor,
                         bottom: nil, trailing: nil,
                         paddingTop: 10, paddingleft: 12,
                         paddingBottom: 0, paddingRight: 0,
                         width: 0, height: 0,
                         centerX: nil, centerY: nil)
        
        imageViewPost.anchor(top: labelDesc.bottomAnchor,
                             leading: self.leadingAnchor,
                             bottom: self.bottomAnchor,
                             trailing: self.trailingAnchor,
                             paddingTop: 10, paddingleft: 12,
                             paddingBottom: 10, paddingRight: 12,
                             width: 0, height: 0,
                             centerX: nil, centerY: nil)
        
        buttonEdit.anchor(top: labelName.topAnchor,
                          leading: nil, bottom: nil,
                          trailing: self.trailingAnchor,
                          paddingTop: 0, paddingleft: 0,
                          paddingBottom: 0, paddingRight: 12,
                          width: 0, height: 0,
                          centerX: nil, centerY: nil)
    }
    
    private func updateUI() {
        labelName.text = viewModel?.labelName
        labelTitle.text = viewModel?.labelTitle
        labelDesc.text = viewModel?.labelDesc
        buttonEdit.isHidden = !(viewModel?.isSomeUser ?? true)
        imageViewPost.setImageWithBase64String(imageStr: viewModel?.imageStr)
    }
    
    private func openPostEditVC(post : Post) {
        let viewModel = PostVM()
        viewModel.activePost = post
        let controller = PostVC(viewModel: viewModel)
        SplashManager.shared.createNavController(viewController: controller)
    }
    
    //MARK: - Actions
    @objc func buttonEditTapped() {
        guard let post = viewModel?.groupList.post else { return }
        self.openPostEditVC(post: post)
    }
}
