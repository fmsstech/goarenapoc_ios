//
//  HomeItemVM.swift
//  POCApp
//
//  Created by Yusuf Cinar on 2.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

final class HomeItemVM: BaseVM {
    
    private(set) var groupList: GroupList
    
    init(groupList : GroupList) {
        self.groupList = groupList
    }
    
    var labelName : String? {
        return self.groupList.user?.username
    }
    
    var labelTitle : String? {
        return self.groupList.post?.title
    }
    
    var labelDesc : String? {
        return self.groupList.post?.description
    }
    
    var imageStr : String? {
        let imageStr = self.groupList.post?.imageStr
        return imageStr
    }
    
    var isSomeUser : Bool {
        guard let id = UserDefaults.standard.getLoginResponseModel()?.id else { return false }
        if id == self.groupList.user?.id {
            return true
        }
        return false
    }
}
