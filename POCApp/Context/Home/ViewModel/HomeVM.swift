//
//  HomeVM.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

final class HomeVM: BaseVM {
    
    private var responseModel: HomeResponseModel?
    
    override var screenType: ScreenType {
        .home
    }
    
    func fetchPostList() {
        let requestModel = HomeRequestModel()
        HomeService().setRequestModel(requestModel).response { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                self.responseModel = response
                self.state?(.success)
            case .failure:
                self.state?(.failure(nil))
            }
        }
    }
    
    var numberOfRows: Int {
        return responseModel?.groupList?.count ?? 0
    }
    
    func getItemVM(index: Int) -> HomeItemVM? {
        
        guard let groupList = self.responseModel?.groupList else { return nil }
        if groupList.indices.contains(index) {
            return HomeItemVM(groupList: groupList[index])
        }
        return nil
    }
}
