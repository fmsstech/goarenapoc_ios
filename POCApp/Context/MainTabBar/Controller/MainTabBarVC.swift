//
//  SplashVC.swift
//  POCApp
//
//  Created by Yusuf Çınar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

final class MainTabBarVC : UITabBarController , UITabBarControllerDelegate {
    
    //MARK: - UI Properties
    private let buttonPlus : UIButton = {
        let button = UIButton(type: .system)
        button.addTarget(self, action: #selector(buttonPlusTapped), for: .touchUpInside)
        button.backgroundColor = UIColor.midBlue
        button.setImage(UIImage.add.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .midBlue
        return button
    }()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        tabBar.isTranslucent = false
        tabBar.tintColor = UIColor.midBlue
        tabBar.barTintColor = .white
        configureController()
        configurePlusButton()
        selectedIndex = 0
    }
    
    //MARK: - Private Functions
    private func configurePlusButton() {
        buttonPlus.frame.size = CGSize(width: 48, height: 48)
        buttonPlus.center = CGPoint(x: tabBar.bounds.midX, y: tabBar.bounds.midY - buttonPlus.frame.height / 5 + 4)
        buttonPlus.layer.cornerRadius = 24
        buttonPlus.layer.borderColor = UIColor.black.cgColor
        buttonPlus.layer.borderWidth = 0.5
        buttonPlus.clipsToBounds = true
        buttonPlus.adjustsImageWhenHighlighted = false
        self.tabBar.addSubview(buttonPlus)
    }
    
    private func configureController() {
        
        let homeVC = generateNavigationController(with: HomeVC(),
                                                   title: "Home",
                                                   image: UIImage(named: "home"))
        let dashboardVC = generateNavigationController(with: DashboardVC(),
                                                    title: "Dashboard",
                                                    image: UIImage(named: "dashboard"))
        
        viewControllers = [homeVC , UIViewController() , dashboardVC]
        
        guard let items = tabBar.items else { return  }
        
        for item in items {
            item.imageInsets = .zero
        }
    }
    
    fileprivate func generateNavigationController(with rootViewController : UIViewController ,
                                                  title : String ,
                                                  image : UIImage?) -> UIViewController {
        
        guard let navImage = image else { return UIViewController() }
        
        let navController = UINavigationController(rootViewController: rootViewController)
        rootViewController.navigationItem.title = title
        navController.tabBarItem.image = navImage.withRenderingMode(.alwaysTemplate)
        navController.tabBarItem.title = title
        return navController
    }
    
    func tabBarController(_ tabBarController: UITabBarController,
                          shouldSelect viewController: UIViewController) -> Bool {
        
        let index = self.viewControllers?.firstIndex(of:viewController)
        
        if index == 1 {
            return false
        }
        return true
    }
    
    //MARK: - Actions
    @objc private func buttonPlusTapped(){
        let vc = PostVC(viewModel: PostVM())
        SplashManager.shared.createNavController(viewController: vc)
    }
}
