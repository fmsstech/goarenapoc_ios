//
//  BaseVM.swift
//  POCApp
//
//  Created by Yusuf Çınar on 31.01.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

class BaseView: UIView {

    //MARK: - UI Properties
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .clear
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()

    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.backgroundColor = .clear
        stackView.spacing = 20
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.layoutMargins = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Override Functions
    func configureUI() { }
    
    //MARK: - Private Functions
    func configureScrollView() {
        if subviews.contains(scrollView) {
            scrollView.removeFromSuperview()
        }
        addSubview(scrollView)
        scrollView.addSubview(stackView)
        scrollView.anchor(top: topAnchor,
                          leading: leadingAnchor,
                          bottom: bottomAnchor,
                          trailing: trailingAnchor,
                          paddingTop: 0,
                          paddingleft: 0,
                          paddingBottom: 0,
                          paddingRight: 0,
                          width: 0,
                          height: 0,
                          centerX: nil,
                          centerY: nil)
        stackView.anchor(top: scrollView.topAnchor,
                         leading: scrollView.leadingAnchor,
                         bottom: scrollView.bottomAnchor,
                         trailing: scrollView.trailingAnchor,
                         paddingTop: 0,
                         paddingleft: 20,
                         paddingBottom: 0,
                         paddingRight: 20,
                         width: 0,
                         height: 0,
                         centerX: nil,
                         centerY: nil)
        stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1 , constant: -40).isActive = true
        setupStackView()
    }
    
    func setupStackView() {
        stackView.arrangedSubviews.forEach({$0.removeFromSuperview()})
    }
}
