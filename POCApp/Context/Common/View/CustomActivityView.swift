//
//  CustomActivityView.swift
//  POCApp
//
//  Created by Yusuf Cinar on 2.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class CustomActivityView: BaseView {

    //MARK: - Properties
    //MARK: - IBOutlets
    //MARK: - UI Properties
    private let activityIndicator : NVActivityIndicatorView = {
        let indicator = NVActivityIndicatorView(frame: .zero,
                                                type: NVActivityIndicatorType.circleStrokeSpin,
                                                color: .white, padding: 0)
        indicator.startAnimating()
        indicator.color = .midBlue
        return indicator
    }()
    
    private let blankView : UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = UIColor(white: 0, alpha: 0.1)
        return view
    }()
    
    //MARK: - Override Functions
    override func configureUI() {
        super.configureUI()
        
        self.addSubview(blankView)
        self.addSubview(activityIndicator)
        blankView.fillSuperView()
        activityIndicator.anchor(top: nil,
                                 leading: nil,
                                 bottom: nil,
                                 trailing: nil,
                                 paddingTop: 0, paddingleft: 0,
                                 paddingBottom: 0, paddingRight: 0,
                                 width: 48, height: 48,
                                 centerX: self.blankView.centerXAnchor,
                                 centerY: nil)
        activityIndicator.centerYAnchor.constraint(equalTo: self.blankView.centerYAnchor).isActive = true
    }
    
    //MARK: - Actions
    func hide() {
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
            self.activityIndicator.alpha = 0
            self.blankView.alpha = 0
        }, completion: { _ in
            self.activityIndicator.stopAnimating()
        })
    }
    
    func show() {
        self.activityIndicator.startAnimating()
        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
            self.activityIndicator.alpha = 1
            self.blankView.alpha = 0.1
        }, completion: nil)
    }
}
