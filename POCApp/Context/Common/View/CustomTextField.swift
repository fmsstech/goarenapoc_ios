//
//  CustomTextField.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {

    let insetLeft :CGFloat = 12
    let insetTop :CGFloat = 4
    let insetBottom :CGFloat = 4
    let insertRight: CGFloat = 0

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: insetTop, left: insetLeft, bottom: insetBottom , right: insertRight))
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: insetTop, left: insetLeft, bottom: insetBottom , right: insertRight))
    }
    
    override var tintColor: UIColor! {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureStyle(){
        self.layer.borderColor = UIColor.lightGrey.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 4
        self.textColor = .sapphire
        self.font =  UIFont.systemFont(ofSize: 13, weight: .medium)
        self.backgroundColor = .whiteSmoke
        self.clipsToBounds = true
    }
}
