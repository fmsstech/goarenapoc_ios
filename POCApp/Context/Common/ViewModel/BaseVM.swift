//
//  BaseVM.swift
//  POCApp
//
//  Created by Yusuf Çınar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

enum ScreenType {
    
    case undefined
    case home
    case dashboard
    case createPost
    case login
    
    var screenName: String {
        switch self {
        case .undefined:
            return "Undefined"
        case .home:
            return "Home Page"
        case .dashboard:
            return "Dashboard"
        case .createPost:
            return "Post Detail"
        case .login:
            return "Login Page"
        }
    }
}


enum StateType : Equatable {
    
    case success
    case failure(Error?)
    
    static func == (lhs: StateType, rhs: StateType) -> Bool {
        switch (lhs, rhs) {
        case (.failure, .failure):
            return true
        case (.success, .success):
            return true
        default:
            return false
        }
    }
}

typealias ClosureState = ((_ state : StateType)->())

class BaseVM: NSObject {
    
    var screenType : ScreenType {
        .undefined
    }
    
    var state : ClosureState?
        
    override init() {
        super.init()
        configureInit()
    }
    
    func configureInit() { }
}
