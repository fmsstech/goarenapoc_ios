//
//  ViewController.swift
//  POCApp
//
//  Created by Yusuf Çınar on 30.01.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

class BaseVC : UIViewController {

    // MARK: - UI Properties
    private let viewNavigation : UIView = UIView()
    private let viewContainer : UIView = UIView()
    private let viewBorder: UIView = UIView()

    private let labelTitle: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize : 16)
        lbl.numberOfLines = 0
        return lbl
    }()

    private lazy var buttonBack : UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Geri", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize : 16)
        button.setImage(UIImage(named: "keyboardArrowLeft24Px")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.addTarget(self, action: #selector(buttonBackTapped), for: .touchUpInside)
        return button
    }()

    private lazy var buttonClose : UIButton = {
        let button = UIButton(type: .system)
        button.addTarget(self, action: #selector(buttonCloseTapped), for: .touchUpInside)
        button.setImage(UIImage(named: "closeIcon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        return button
    }()

    // MARK: - Properties
    var headerBottomAnchor: NSLayoutYAxisAnchor {
        return viewNavigation.bottomAnchor
    }

    var headerTitle: String = "" {
        didSet {
            labelTitle.text = headerTitle
        }
    }

    var headerTitleColor: UIColor {
        return UIColor.black
    }

    var navBarColor: UIColor {
        return UIColor.white
    }
    
    var viewBorderColor: UIColor {
        return UIColor(white: 0.5, alpha: 0.5)
    }

    var isCloseButtonHidden: Bool {
        return true
    }

    var isBackButtonHidden: Bool {
        return true
    }

    var closeButtonColor: UIColor {
        return UIColor.black
    }

    var backButtonColor: UIColor {
        return UIColor.black
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureHeader()
        configureStyleView()
        configureUI()
        self.navigationController?.navigationBar.isHidden = true
    }

    // MARK: - Private Functions
    private func configureStyleView(){
        self.view.backgroundColor = .white
        viewNavigation.backgroundColor = navBarColor
        viewBorder.backgroundColor = viewBorderColor
        labelTitle.textColor = headerTitleColor
        buttonBack.tintColor = backButtonColor
        buttonClose.tintColor = closeButtonColor
        buttonBack.isHidden = isBackButtonHidden
        buttonClose.isHidden = isCloseButtonHidden
    }

    private func configureHeader() {
        
        let heightLayout = UIApplication.shared.windows.first{ $0.isKeyWindow }?.safeAreaInsets.top ?? 0
        let heightNavBar = self.navigationController?.navigationBar.frame.size.height ?? 44

        view.addSubview(viewNavigation)
        viewNavigation.addSubview(viewContainer)
        viewNavigation.addSubview(viewBorder)
        viewContainer.addSubview(buttonBack)
        viewContainer.addSubview(buttonClose)
        viewContainer.addSubview(labelTitle)
        
        viewNavigation.anchor(top: view.topAnchor,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: view.trailingAnchor,
                             paddingTop: 0,
                             paddingleft: 0,
                             paddingBottom: 0,
                             paddingRight: 0,
                             width: 0,
                             height: heightLayout + heightNavBar,
                             centerX: nil,
                             centerY: nil)
        
        viewContainer.anchor(top: nil,
                                 leading: viewNavigation.safeAreaLayoutGuide.leadingAnchor,
                                 bottom: viewNavigation.bottomAnchor,
                                 trailing: viewNavigation.safeAreaLayoutGuide.trailingAnchor,
                                 paddingTop: 0,
                                 paddingleft: 4,
                                 paddingBottom: 0.5,
                                 paddingRight: 12,
                                 width: 0,
                                 height: heightNavBar,
                                 centerX: nil,
                                 centerY: nil)
        
        buttonBack.anchor(top: nil,
                          leading: viewContainer.leadingAnchor,
                          bottom: nil,
                          trailing: nil,
                          paddingTop: 0,
                          paddingleft: 0,
                          paddingBottom: 0,
                          paddingRight: 0,
                          width: 0,
                          height: 0,
                          centerX: nil,
                          centerY: viewContainer.centerYAnchor)
        
        buttonClose.anchor(top: nil,
                           leading: nil,
                           bottom: nil,
                           trailing: viewContainer.trailingAnchor,
                           paddingTop: 0,
                           paddingleft: 0,
                           paddingBottom: 0,
                           paddingRight: 0,
                           width: 0,
                           height: 0,
                           centerX: nil,
                           centerY: viewContainer.centerYAnchor)
        
        labelTitle.anchor(top: nil,
                          leading: nil,
                          bottom: nil,
                          trailing: nil,
                          paddingTop: 0,
                          paddingleft: 0,
                          paddingBottom: 0,
                          paddingRight: 0,
                          width: 0,
                          height: 0,
                          centerX: viewContainer.centerXAnchor,
                          centerY: viewContainer.centerYAnchor)
        
        viewBorder.anchor(top: nil,
                          leading: viewNavigation.leadingAnchor,
                          bottom: viewNavigation.bottomAnchor,
                          trailing: viewNavigation.trailingAnchor,
                          paddingTop: 0,
                          paddingleft: 0,
                          paddingBottom: 0,
                          paddingRight: 0,
                          width: 0,
                          height: 0.5,
                          centerX: nil,
                          centerY: nil)
    }

    // MARK: - Actions
    @objc func buttonCloseTapped() {
        dismiss(animated: true, completion: nil)
    }

    @objc func buttonBackTapped() {
        navigationController?.popViewController(animated: true)
    }

    func popToViewController(isMember ofClass: AnyClass, animated: Bool = false) {
        guard let navigationController = navigationController else { return }
        if let vc = navigationController.viewControllers.last(where: { $0.isMember(of: ofClass) }) {
            navigationController.popToViewController(vc, animated: animated)
        }
    }
    
    //MARK: - Override Functions
    func configureUI() { }
}
