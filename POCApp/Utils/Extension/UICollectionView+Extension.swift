//
//  UICollectionView+Extension.swift
//  POCApp
//
//  Created by Yusuf Cinar on 2.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

protocol ReusableCell: NSObjectProtocol {
    static var defaultIdentifier: String { get }
}

extension ReusableCell where Self: UICollectionReusableView {
    
    static var defaultIdentifier: String {
        return String(describing: self)
    }
}

extension UICollectionReusableView: ReusableCell { }

extension UICollectionView {
    
    final func registerCell<T: UICollectionViewCell>(cellType: T.Type){
        self.register(cellType.self, forCellWithReuseIdentifier: cellType.defaultIdentifier)
    }
    
    final func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath, cellType: T.Type) -> T {
        let bareCell = self.dequeueReusableCell(withReuseIdentifier: cellType.defaultIdentifier, for: indexPath)
        guard let cell = bareCell as? T else {
          fatalError(
            "Failed to dequeue a cell with identifier \(cellType.defaultIdentifier) matching type \(cellType.self)."
          )
        }
        return cell
    }
    
    final func dequeueReusableSupplementaryView<T: UICollectionReusableView>
      (ofKind elementKind: String, for indexPath: IndexPath, viewType: T.Type) -> T {
        let view = self.dequeueReusableSupplementaryView(ofKind: elementKind,
                                                         withReuseIdentifier: viewType.defaultIdentifier,
                                                         for: indexPath)
        guard let typedView = view as? T else {
          fatalError(
            "Failed to dequeue a supplementary view with identifier \(viewType.defaultIdentifier) "
              + "matching type \(viewType.self). "
          )
        }
        return typedView
    }
}
