//
//  UIImageView+Extension.swift
//  POCApp
//
//  Created by Yusuf Cinar on 2.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func setImageWithBase64String(imageStr : String?) {
        guard let imageStr = imageStr else { return }
        let dataDecoded : Data = Data(base64Encoded: imageStr, options: .ignoreUnknownCharacters)!
        self.image = UIImage(data: dataDecoded)
    }
}
