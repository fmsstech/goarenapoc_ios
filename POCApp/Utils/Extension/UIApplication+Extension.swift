//
//  UIApplication+Extension.swift
//  POCApp
//
//  Created by Yusuf Cinar on 2.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

extension UIApplication {
    
    var keyWindowInConnectedScenes: UIWindow? {
        return windows.first(where: { $0.isKeyWindow })
    }
    
    class func topViewController(controller: UIViewController? = UIApplication.shared.windows.first?.rootViewController) -> UIViewController? {
        
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let pageViewController = controller as? UIPageViewController {
            if let currentPage = pageViewController.viewControllers?.first {
                return topViewController(controller: currentPage)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    class func rootViewController() -> UIViewController? {
        return UIApplication.shared.windows.first?.rootViewController
    }
}
