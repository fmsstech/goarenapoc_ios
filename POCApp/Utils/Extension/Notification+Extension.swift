//
//  Notification+Extension.swift
//  POCApp
//
//  Created by Yusuf Cinar on 2.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    func addObserver(observer: Any, selector: Selector, object: Any? = nil) {
        NotificationCenter.default.addObserver(observer, selector: selector, name: self, object: object)
    }
    
    func removeObserver(observer: Any, object: Any? = nil) {
        NotificationCenter.default.removeObserver(observer, name: self, object: object)
    }
    
    func post(object: Any? = nil, userInfo: [AnyHashable: Any]? = nil) {
        NotificationCenter.default.post(name: self, object: object, userInfo: userInfo)
    }
}
