//
//  UIColor+Extension.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let baseText : UIColor = {
        return rgb(red: 51 , green: 51, blue: 51)
    }()
    
    static var whiteSmoke : UIColor {
        return rgb(red: 244, green: 244, blue: 246)
    }
    
    static var lightGrey : UIColor {
        return UIColor(red:215/255.0, green:215/255.0, blue:215/255.0,  alpha:1)
    }
    
    static var navyBlue : UIColor {
        return UIColor(red:0/255.0, green:71/255.0, blue:204/255.0,  alpha:1)
    }

    static var midBlue : UIColor {
        return UIColor.rgb(red: 40, green: 85, blue: 172)
    }
    
    static var sapphire : UIColor {
        return UIColor(red:4/255.0, green:44/255.0, blue:92/255.0,  alpha:1)
    }

    //MARK: - Helper
    static func rgb(red :  CGFloat , green :  CGFloat , blue : CGFloat) -> UIColor {
        return  UIColor(red: red / 255, green: green / 255, blue: blue / 255, alpha: 1)
    }
    
    static var random: UIColor {
        let r:CGFloat  = .random(in: 0 ... 1)
        let g:CGFloat  = .random(in: 0 ... 1)
        let b:CGFloat  = .random(in: 0 ... 1)
        return UIColor(red: r, green: g, blue: b, alpha: 1)
    }
}
