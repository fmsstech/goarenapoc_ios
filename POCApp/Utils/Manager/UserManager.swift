//
//  UserManager.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

extension UserDefaults {
    
    enum UserDefaultKeys: String , CaseIterable {
        case token
        case loginResponseModel
    }
    
    //MARK:Token
    func getToken() -> String? {
        let token = object(forKey: UserDefaultKeys.token.rawValue) as? String
        return token
    }
    
    func setToken(value :String){
        set(value, forKey: UserDefaultKeys.token.rawValue)
        synchronize()
    }
    
    func isHaveToken() -> Bool{
        return getToken() == nil ? false : true
    }
    
    @discardableResult
    func removeToken() -> Bool {
        removeObject(forKey: UserDefaultKeys.token.rawValue)
        synchronize()
        return isHaveToken()
    }
        
    // MARK:- LoginResponseModel
    func getLoginResponseModel() -> LoginResponseModel? {
        guard let userData = data(forKey: UserDefaultKeys.loginResponseModel.rawValue) else { return nil }
        do {
            let userModel =  try JSONDecoder().decode(LoginResponseModel.self, from: userData)
            return userModel
        } catch let decoderError {
            print("failed to decoder",decoderError)
        }
        return nil
    }
    
    func saveLoginResponseModel(loginResponseModel: LoginResponseModel){
        do {
            let data = try JSONEncoder().encode(loginResponseModel)
            set(data, forKey: UserDefaultKeys.loginResponseModel.rawValue)
        } catch let encodeError {
            print("Failed to encode ",encodeError)
        }
    }
    
    //MARK: Reset UserDefault
    func resetDefaults(keys : [UserDefaultKeys] = UserDefaultKeys.allCases) {
        keys.forEach ({
            self.removeObject(forKey:$0.rawValue)
            self.synchronize()
        })
    }
}
