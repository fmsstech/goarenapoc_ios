//
//  SplashManager.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

class SplashManager {
    
    private init() { }
    static let shared = SplashManager()
        
    func configureRootVC() {
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        if UserDefaults.standard.isHaveToken() {
            window?.rootViewController = MainTabBarVC()
            return
        }
        else {
            window?.rootViewController = LoginVC()
        }
    }
    
    func createNavController(viewController: BaseVC) {
        guard let topController = UIApplication.topViewController() else { return }
        let navPostVC = UINavigationController(rootViewController:viewController)
        navPostVC.modalPresentationStyle = .fullScreen
        navPostVC.isModalInPresentation = true
        DispatchQueue.main.async {
            topController.present(navPostVC, animated: true, completion: nil)
        }
    }
}
