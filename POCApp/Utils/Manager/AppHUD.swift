//
//  AppHUD.swift
//  POCApp
//
//  Created by Yusuf Çınar on 30.01.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import NVActivityIndicatorView

class AppHUD {
    
    static let shared = AppHUD()
    
    private let activityView : CustomActivityView = CustomActivityView(frame: .zero)
    
    private init(){}
    
    func show(){
        let windowsFirst = UIApplication.shared.windows.first { $0.isKeyWindow }
        guard let window = windowsFirst else { return }
        activityView.frame = window.frame
        window.addSubview(activityView)
    }
    
    func hide(){
        UIView.animate(withDuration: 0.4, delay: 0,
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 0.8,
                       options: .curveLinear, animations: {
            self.activityView.removeFromSuperview()
        })
    }
}
