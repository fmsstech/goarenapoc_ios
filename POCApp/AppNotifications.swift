//
//  AppNotifications.swift
//  POCApp
//
//  Created by Yusuf Cinar on 2.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

struct AppNotifications {
    static let updateHomeVC = Notification.Name("updateHomeVC")
}
