//
//  HeaderUtils.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Alamofire

final class HeaderUtils: NSObject {
    
    static func allDefaultHeaderList() -> HTTPHeaders {
        var defaultHeaders = HTTPHeaders.default
        guard let staticHeaderList = HeaderUtils.staticHeaders() else { return defaultHeaders }
        for header in staticHeaderList {
            defaultHeaders[header.name] = header.value
        }
        return defaultHeaders
    }

    private static func staticHeaders() -> [HTTPHeader]? {
        return []
    }
    
    static func dynamicHeaders() -> [HTTPHeader] {
        var headers : [HTTPHeader] = []
        if UserDefaults.standard.isHaveToken() {
            headers.append(HTTPHeader(name: "Authorization",
                                   value: "Bearer " + UserDefaults.standard.getToken()!))
        }
        return headers
    }
}
