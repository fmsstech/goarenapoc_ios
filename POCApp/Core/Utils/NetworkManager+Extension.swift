//
//  NetworkManager+Extension.swift
//  POCApp
//
//  Created by Yusuf Çınar on 30.01.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Alamofire
import OHHTTPStubs

extension NetworkManager {
    
    func log<Request,Response>(_ service : BaseService<Request,Response>,
                               _ response: DataResponse<Any, AFError>?) {
        guard let data = try? JSONEncoder().encode(service.requestModel) else { return }
        let dict = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any]
        NetworkLogger.printNetworkCall(url: service.path,
                                       methodType: service.method,
                                       body: dict,response: response?.value,
                                       headers: response?.request?.headers,
                                       statusCode: response?.response?.statusCode)
    }
    
    // MARK: OHHTTPStubs Activation
    func activateStub() {
        OHHTTPStubs.onStubActivation({ (request, stub, _) in
            print("[OHHTTPStubs] Request to \(request.url!) has been stubbed with \(String(describing: stub.name))")
        })
        stub(condition: { req in
            (req.url?.pathExtension) == "json"}) { request in
                    if let arr: Array = request.url?.absoluteString.components(separatedBy: "/") {
                        if arr.count > 0 {
                            let stubPath = OHPathForFile(arr.last!.components(separatedBy: "?").first!, NetworkManager.self)
                            if let stubPath = stubPath {
                                return fixture(filePath: stubPath, headers:
                                    ["Content-Type" as NSObject: "application/json; charset=utf-8" as AnyObject])
                                    .requestTime(1.0, responseTime: 0.2)
                            }
                        }
                    }
            return OHHTTPStubsResponse()
        }
    }
}
