//
//  NetworkLogger.swift
//  POCApp
//
//  Created by Yusuf Çınar on 30.01.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Alamofire

class NetworkLogger {
    class func printNetworkCall(url: String?,
                                methodType: HTTPMethod?,
                                body: [String: Any]?,
                                response: Any?,
                                headers: HTTPHeaders? = nil,
                                statusCode: Int?) {
        #if DEBUG
        print("""
            --------------------------------------------------
            ☆ Request Url : \(url ?? "")
            ☆ Request Type : \((methodType as AnyObject))
            ☆ Request Headers: \(headers?.description ?? "")
            ☆ Request Parameters : \((body as AnyObject))
            ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽
            ◁ Response
            ◁ Status Code: \(statusCode ?? -1)
            ◁ \n\((response as AnyObject))
            ◁
            --------------------------------------------------
            """)
        #endif
    }
}
