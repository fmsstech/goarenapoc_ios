//
//  NetworkManager.swift
//  POCApp
//
//  Created by Yusuf Çınar on 30.01.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Alamofire

enum ReachabilityType: String {
    case wifi
    case cellular
    case none
}

enum ContextType {
    case fmss
    case report
}

final class NetworkManager : NSObject {

    static let shared : NetworkManager = NetworkManager()
    private var manager : Session!
    private var disconnected: Bool = false

    // MARK: Reachability Manager
    private(set) var reachabilityManager: NetworkReachabilityManager!

    private var reachabilityType: ReachabilityType {
        guard reachabilityManager.isReachable else { return .none }
        if reachabilityManager.isReachableOnCellular {
            return .cellular
        }
        return .wifi
    }

    // MARK: Initialize
    override init() {
        super.init()
        self.configureReachability()
        self.configureAlamofireManager()
        
        if AppSettings.STUBSERVICE {
            self.activateStub()
        }
    }

    // MARK: Configurations
    func configureReachability() {
        reachabilityManager = NetworkReachabilityManager(host: "www.google.com")
        reachabilityManager.startListening { [weak self] (status) in
            guard let self = self else { return }

            if case .reachable = status, self.disconnected == true {
                self.disconnected = false
               print("Yeniden bağlandı")
            }
            if status == .notReachable {
                AppHUD.shared.show()
                self.disconnected = true
                print("Bağlantı hatası")
            }
        }
    }

    private func configureAlamofireManager() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = AppSettings.getEnvironment().requestTimeout
        configuration.timeoutIntervalForResource = AppSettings.getEnvironment().responseTimeout
        configuration.headers = HTTPHeaders.default
        var agent: String = UIDevice.current.identifierForVendor?.uuidString ?? ""
        agent.append(" ")
        agent.append(UIDevice.current.systemVersion)
        agent.append(" ")
        agent.append(UIDevice.current.systemName)
        agent.append(" Mobile;App=FMSS")
        agent.append((Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String)! + ")")
        configuration.httpAdditionalHeaders = ["User-Agent": agent]
        manager = Session(configuration: configuration,
                          delegate: SessionDelegate(),
                          rootQueue: DispatchQueue.main)
    }

    func requestJSON<Request,Response> (_ service: BaseService<Request,Response>,
                                        completionHandler: @escaping (DataResponse<Any, AFError>) -> Void) {
        manager.request(service)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response) in
                completionHandler(response)
        }
    }

    func requestHtml<Request,Response> (_ service: BaseService<Request,Response>,
                                        completionHandler: @escaping (DataResponse<Any, AFError>) -> Void) {
        manager.request(service)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["text/html"])
            .responseJSON { response in
                completionHandler(response)
        }
    }
}
