//
//  ResponseModel.swift
//  POCApp
//
//  Created by Yusuf Cinar on 1.02.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

struct StatusModel : Decodable {
    let code: String?
    let fail: Bool?
    let message: String?
    let success: Bool?
}

struct ResponseModel<T:Decodable>: Decodable  {

    let data: T?
    let status: StatusModel?

    enum CodingKeys: String, CodingKey {
        case data
        case status
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        data = try? container.decode(T.self, forKey: .data)
        status = try? container.decode(StatusModel.self, forKey: .status)
    }
}
