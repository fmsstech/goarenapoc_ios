//
//  BaseService.swift
//  POCApp
//
//  Created by Yusuf Çınar on 30.01.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Alamofire

enum ErrorType: Error {
    case unReachable
    case statusCode(_ statusCode: Int)
    case domain
    case decoding
    case empty
}

protocol BaseRequestProtocol: Encodable {

}

struct BaseRequestModel : BaseRequestProtocol {

}

protocol APIConfiguration {
    associatedtype RequestModel
    var baseUrl : String { get }
    var headers : [HTTPHeader]? { get }
    var method : Alamofire.HTTPMethod{ get}
    var requestModel : RequestModel { get }
    var path : String { get }
    var encoding : Alamofire.ParameterEncoding?{ get }
    var contextType : ContextType { get }
}

class BaseService <BaseRequestModel: BaseRequestProtocol , BaseResponseModel: Decodable > : URLRequestConvertible, APIConfiguration {

    typealias ResultBlock = (Swift.Result<BaseResponseModel, ErrorType>) -> ()

    private var resultBlock : ResultBlock!
    var baseUrl : String =  AppSettings.getEnvironment().backendUrl
    var headers : [HTTPHeader]? = HeaderUtils.dynamicHeaders()
    var method: HTTPMethod = .post
    var requestModel : BaseRequestModel?
    var path: String = String()
    var showActivityIndicator: Bool = false
    var encoding: ParameterEncoding? = URLEncoding.httpBody
    var contextType: ContextType = .fmss

    @discardableResult
    func setRequestModel(_ requestModel : RequestModel) -> Self {
        self.requestModel = requestModel
        return self
    }

    @discardableResult
    func setIsShowLoading(_ value : Bool) -> Self {
        self.showActivityIndicator = value
        return self
    }

    @discardableResult
    func setUrlPath(_ lastString : String) -> Self {
        self.path = lastString
        return self
    }

    @discardableResult
    func addParameterToUrlPath(_ parameterString : String) -> Self {
        self.path = self.path + "/\(parameterString)"
        return self
    }

    @discardableResult
    func setRequestType(_ method : HTTPMethod) -> Self {
        self.method = method
        return self
    }

    @discardableResult
    func setEncodingtype(_ type : URLEncoding) -> Self {
        self.encoding = type
        return self
    }

    @discardableResult
    func setContextType(type : ContextType) ->Self {
        self.contextType = type
        return self
    }

    internal func asURLRequest() throws -> URLRequest {
        configureUrl()
        let url =  try baseUrl.asURL()
        var urlRequest  = URLRequest(url: url.appendingPathComponent(path))
        if path.count == 0 {
            urlRequest = URLRequest(url: url)
        }
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if let headers = self.headers {
            headers.forEach { (header) in
                urlRequest.setValue(header.value, forHTTPHeaderField: header.name)
            }
        }
        
        if method == .post {
            urlRequest.httpBody = try? JSONEncoder().encode(requestModel)
            return urlRequest
        }

        if let data = try? JSONEncoder().encode(requestModel) {
            let dict = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any]
            return try (encoding?.encode(urlRequest, with: dict) ?? urlRequest)
        }
        return urlRequest
    }

    func response(resultBlock: @escaping ResultBlock) {
        self.resultBlock = resultBlock
        configureRequest()
    }

    private func configureRequest() {
        if NetworkManager.shared.reachabilityManager.isReachable {
            showLoading()
            sendRequest()
        } else {
            self.resultBlock(.failure(.unReachable))
        }
    }

    private func sendRequest() {
        NetworkManager.shared.requestJSON(self){(response) in
            self.hideLoading()
            NetworkManager.shared.log(self, response)
            guard let responseData = response.data  else {
                self.resultBlock(.failure(.domain))
                return
            }
            do {
                let baseResponse = try JSONDecoder().decode(ResponseModel<BaseResponseModel>.self, from: responseData)
                if let content = baseResponse.data {
                    self.resultBlock(.success(content))
                    return
                }
                self.resultBlock(.failure(.empty))
            } catch  {
                self.resultBlock(.failure(.decoding))
            }
        }
    }

    private func configureUrl() {
        switch contextType {
        case .fmss:
            print("FMSS Context")
        default:
            break
        }
    }

    private func hideLoading(){
        if showActivityIndicator {
            DispatchQueue.main.async {
                AppHUD.shared.hide()
            }           
        }
    }

    private func showLoading(){
        if showActivityIndicator {
            DispatchQueue.main.async {
                AppHUD.shared.show()
            }
        }
    }
}
