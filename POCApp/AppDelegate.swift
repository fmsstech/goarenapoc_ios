//
//  AppDelegate.swift
//  POCApp
//
//  Created by Yusuf Çınar on 30.01.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions
                        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        configureRootVC()
        configureIQKeyboard()
        return true
    }

    func configureRootVC(){
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        UserDefaults.standard.resetDefaults()
        SplashManager.shared.configureRootVC()
    }
    
    private func configureIQKeyboard() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldToolbarUsesTextFieldTintColor = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 10.0
    }
}
