//
//  AppSettings.swift
//  POCApp
//
//  Created by Yusuf Çınar on 30.01.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import UIKit

enum EnvironmentType: String , CaseIterable {
    case prod
    case test
}

class EnvironmentModel: NSObject {

    var backendUrl: String = ""
    var requestTimeout: Double = 20
    var responseTimeout: Double = 20
}

class AppSettings: NSObject {

    static let STUBSERVICE: Bool = false
    private static var environmentMap = [EnvironmentType: EnvironmentModel]()

    static var currentEnvironment: EnvironmentType {
        get {
            if AppSettings.environmentMap.count == 0 {
                AppSettings.populateEnvironmentObjects()
            }
            return EnvironmentType.allCases.first ?? .prod
        }
        set(type) {
            DispatchQueue.main.async {
                //configureRootVC()
            }
        }
    }

    static func getEnvironment() -> EnvironmentModel {
        if AppSettings.environmentMap.count == 0 {
            AppSettings.populateEnvironmentObjects()
        }
        return environmentMap[currentEnvironment]!
    }

    private static func populateEnvironmentObjects() {
        AppSettings.environmentMap.removeAll()

        let prod = EnvironmentModel()
        prod.backendUrl = AppConstants.API_PROD_URL

        let test = EnvironmentModel()
        test.backendUrl = AppConstants.API_TEST_URL

        AppSettings.environmentMap[.prod] = prod
        AppSettings.environmentMap[.test] = test
    }
}
