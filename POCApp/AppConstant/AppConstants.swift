//
//  AppConstants.swift
//  POCApp
//
//  Created by Yusuf Çınar on 30.01.2021.
//  Copyright © 2021 FMSS. All rights reserved.
//

import Foundation

struct AppConstants {

    static let API_PROD_URL: String = "http://3.133.148.86:8060/"
    static let API_TEST_URL: String = "http://3.133.148.86:8060/"
}
